#!/bin/bash

mkdir -p fat_libs
lipo bgfx/.build/ios-arm/bin/libbxRelease.a bgfx/.build/ios-arm64/bin/libbxRelease.a bgfx/.build/ios-simulator/bin/libbxRelease.a bgfx/.build/ios-simulator64/bin/libbxRelease.a -create -output fat_libs/libbxRelease.a
lipo bgfx/.build/ios-arm/bin/libbimgRelease.a bgfx/.build/ios-arm64/bin/libbimgRelease.a bgfx/.build/ios-simulator/bin/libbimgRelease.a bgfx/.build/ios-simulator64/bin/libbimgRelease.a -create -output fat_libs/libbimgRelease.a
lipo bgfx/.build/ios-arm/bin/libbgfxRelease.a bgfx/.build/ios-arm64/bin/libbgfxRelease.a bgfx/.build/ios-simulator/bin/libbgfxRelease.a bgfx/.build/ios-simulator64/bin/libbgfxRelease.a -create -output fat_libs/libbgfxRelease.a
