//
//  TapittyTapTap.h
//  TapittyTapTap
//
//  Created by Dustin Wenz on 10/14/16.
//  Copyright © 2016 Dustin Wenz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TapittyTapTap : NSObject

+ (void)tap:(int)thetap;

@end
